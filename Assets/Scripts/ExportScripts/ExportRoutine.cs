﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class ExportRoutine : MonoBehaviour
{
    [SerializeField] private SpawnItemList m_itemList;

    private AssetReferenceGameObject m_assetLoadedAsset;
    private GameObject m_instanceObject = null;
    private int carIndex;

    public int captureWidth = 512;
    public int captureHeight = 512;
    private string outputFolder;
    private Rect rect;
    private RenderTexture renderTexture;
    private Texture2D screenShot;
    private bool isProcessing;
    private bool isloading;
    private int imageNum = 0;
    private void Start()
    {
        if (m_itemList == null || m_itemList.AssetReferenceCount == 0)
        {
            Debug.LogError("Spawn list not setup correctly");
        }
        else
        {
            StartCoroutine( RunAutoCamera());
        }

    }
    IEnumerator RunAutoCamera()
    {
        for (int i = 0; i < m_itemList.AssetReferenceCount; i++)
        {
            isloading = true;
            ChangeCarIndexWithValue(i);
            
            while (m_instanceObject == null)
            {
                yield return null;
            }
            imageNum = 0;
            isProcessing = true;
            StartCoroutine(CaptureScreenshot());
            while (isProcessing == true)
            {
                yield return null;
            }
        }
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
         
#endif
    }
    public void LoadItemAtIndex(SpawnItemList itemList, int index)
    {
        //isloading = true;
        if (m_instanceObject != null)
        {
            Destroy(m_instanceObject);
        }

        m_assetLoadedAsset = itemList.GetAssetReferenceAtIndex(index);
        var spawnPosition = new Vector3();
        var spawnRotation = Quaternion.Euler(0,90,0);
        var parentTransform = this.transform;


        var loadRoutine = m_assetLoadedAsset.LoadAssetAsync();
        loadRoutine.Completed += LoadRoutine_Completed;

        void LoadRoutine_Completed(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<GameObject> obj)
        {
            m_instanceObject = Instantiate(obj.Result, spawnPosition, spawnRotation, parentTransform);
            if (m_instanceObject)
            {
                FocusOn(Camera.main, m_instanceObject, 1.6f);//when the model is loaded the camera is focused on the item
            }
        }
        isloading = false;
    }
    public void ChangeCarIndex(int index)
    {
        carIndex += index;//change the index based on incremented values
        if (carIndex > m_itemList.AssetReferenceCount - 1)//if the index is higher that the asset count reset it to zero
        {

            carIndex = 0;
        }
        else if (carIndex < 0)//if the index is lower than zero skip to the max count of the list
        {
            carIndex = m_itemList.AssetReferenceCount - 1;
        }
        if (m_instanceObject)
        {
            DestroyImmediate(m_instanceObject);//destroy the last model that is instantiated so that the models dont stack on each other.
        }
        LoadItemAtIndex(m_itemList, carIndex);//load the model based on the index

    }
    public void ChangeCarIndexWithValue(int index)
    {
        //isloading = true;
        if (m_instanceObject)
        {
            DestroyImmediate(m_instanceObject);//destroy the last model that is instantiated so that the models dont stack on each other.
        }
        if (carIndex > m_itemList.AssetReferenceCount - 1)//if the index is higher that the asset count reset it to zero
        {

            carIndex = 0;
        }
        else if (carIndex < 0)//if the index is lower than zero skip to the max count of the list
        {
            carIndex = m_itemList.AssetReferenceCount - 1;
        }
        
        
        LoadItemAtIndex(m_itemList, index);//load the model based on the index

    }
    public void RunPhotosequence()
    {
        if (m_instanceObject)
        {
            TakeScreenShot();
        }
        else
        {
            Debug.LogError("Select a vehicle first");
        }
    }
    public void DeleteVehicle()
    {
        if (m_instanceObject)
        {
            DestroyImmediate(m_instanceObject);
        }
        else
        {
            Debug.Log("No Vehicle to Destroy");
        }
    }
    
    public Bounds GetBoundsWithChildren(GameObject focusedObject)
    {
        Renderer[] renderers = focusedObject.GetComponentsInChildren<Renderer>();
        Bounds bounds = renderers.Length > 0 ? renderers[0].bounds : new Bounds();
        for (int i = 1; i < renderers.Length; i++)
        {
            if (renderers[i].enabled)
            {
                bounds.Encapsulate(renderers[i].bounds);//find the sizes of all the renderers int he scene
            }
        }

        return bounds;
    }

    public void FocusOn(Camera camera, GameObject focusedObject, float marginPercentage)
    {
        Bounds bounds = GetBoundsWithChildren(focusedObject);
        float maxExtent = bounds.extents.magnitude;
        float minDistance = (maxExtent * marginPercentage) / Mathf.Sin(Mathf.Deg2Rad * camera.fieldOfView / 2f);
        camera.transform.position = focusedObject.transform.position - Vector3.forward * minDistance;
        camera.transform.position = new Vector3(camera.transform.position.x, bounds.center.y, camera.transform.position.z);// move the position of the camera in order to make sure i can see the whole model
        if (bounds.size.x > bounds.size.y && bounds.size.x > bounds.size.z)//checks the bounding sizes so that the camera size can see the entire model
        {
            camera.orthographicSize = bounds.size.x/ marginPercentage;
            camera.transform.GetChild(0).GetComponent<Camera>().orthographicSize = bounds.size.x/ marginPercentage;
        }else if (bounds.size.y > bounds.size.x && bounds.size.y > bounds.size.z)
        {
            camera.orthographicSize = bounds.size.y/ marginPercentage;
            camera.transform.GetChild(0).GetComponent<Camera>().orthographicSize = bounds.size.y / marginPercentage;
        }
        else if (bounds.size.z > bounds.size.x && bounds.size.z > bounds.size.y)
        {
            camera.orthographicSize = bounds.size.z/ marginPercentage;
            camera.transform.GetChild(0).GetComponent<Camera>().orthographicSize = bounds.size.z / marginPercentage;
        }
    }

    private IEnumerator CaptureScreenshot()
    {
        isProcessing = true;
        float rotationY = 90;
        for (int i = 0; i < 16; i++)//counts up to 16 for all the screenshots
        {
            rotationY += 22.5f;//changes the rotation value of the model by 22.5
            outputFolder = System.IO.Directory.GetCurrentDirectory() + "/Output/" + m_instanceObject.name.Replace("(Clone)", "") + "/";//sets the folder names and gets rid of the clone tag on the prefab
            if (!Directory.Exists(outputFolder))
            {
                Directory.CreateDirectory(outputFolder);//creates a folder based on the name of the model if it doesnt already exist
                Debug.Log("Save Path will be : " + outputFolder);
            }
            if (renderTexture == null)
            {
                rect = new Rect(0, 0, captureWidth, captureHeight);
                renderTexture = new RenderTexture(captureWidth, captureHeight, 24);
                screenShot = new Texture2D(captureWidth, captureHeight, TextureFormat.RGBA32, true);
            }

            Camera camera = Camera.main;
            camera.targetTexture = renderTexture;
            camera.Render();
            RenderTexture.active = renderTexture;
            screenShot.ReadPixels(rect, 0, 0);

            camera.targetTexture = null;
            RenderTexture.active = null;

            string filename = string.Format("frame" + imageNum.ToString("0000") + ".png", outputFolder, ".png"); ;


            byte[] fileHeader = null;
            byte[] fileData = null;

            fileData = screenShot.EncodeToPNG();//changes the file type to png, although without the .png tag at the end it will just be an unformatted file
            new System.Threading.Thread(() =>
            {
            var f = System.IO.File.Create(outputFolder + filename);
                if (fileHeader != null) f.Write(fileHeader, 0, fileHeader.Length);
                f.Write(fileData, 0, fileData.Length);
                f.Close();
                Debug.Log(string.Format("Wrote screenshot {0} of size {1}", filename, fileData.Length));
            }).Start();

            
            Destroy(renderTexture);
            renderTexture = null;
            screenShot = null;
            imageNum++;//iterates the file number value

            m_instanceObject.transform.rotation = Quaternion.Euler(m_instanceObject.transform.rotation.x, rotationY, m_instanceObject.transform.rotation.z);//spins the model
            
            yield return new WaitForSeconds(0f);
        }
        isProcessing = false;
    }
    public void TakeScreenShot()
    {
        if (!isProcessing)
        {
            imageNum = 0;
            StartCoroutine( CaptureScreenshot());
            
        }
        else
        {
            Debug.Log("Currently Processing");
        }
    }
}
